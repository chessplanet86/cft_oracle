# Файл фомирования таблиц shops и Employees

import cx_Oracle
import random
import functools

a = functools.partial(random.randint, 0, 9)
gen = lambda: "+7-{}{}{}-{}{}{}-{}{}{}{}".format(a(), a(), a(), a(), a(), a(), a(), a(), a(), a())


def gen_director():
    db = cx_Oracle.connect("ALEXEY", "masterkey", "127.0.0.1:1521/orcl.ofoms.local", encoding="UTF-8")
    # db.autocommit = True
    cursor = db.cursor()
    regs = open("reg.txt", "r", encoding="utf8")
    streets = open("streets.txt", "r", encoding="utf8")
    shops = open("shops.txt", "r", encoding="utf8")
    cities = open("cities.txt", "r", encoding="utf8")
    fnames = open("surnames.txt", "r", encoding="utf8")
    lnames = open("names.txt", "r", encoding="utf8")
    mails = open("mail.txt", "r", encoding="utf8")

    manager_id_array = [i for i in range(1, 252)]
    random.shuffle(manager_id_array)  # перетасовываем
    cursor.execute("""select table_name, comments from user_tab_comments""")

    for i in range(0, 251):
        cursor.execute(
            """INSERT INTO SHOPS (ID, NAME, CITY, REGION, ADDRESS, MANAGER_ID) VALUES (:id, :name, :city, :reg, :addr, :manager_id)""",
            id=i + 1,
            name=shops.readline().strip(),
            city=cities.readline().strip(),
            reg=regs.readline().strip(),
            addr=f"ул.{streets.readline().strip()} д.{random.randint(1, 250)}",
            manager_id=manager_id_array[i],
        )
        cursor.execute(
            """INSERT INTO EMPLOYEES (ID, FIRST_NAME, LAST_NAME, PHONE, E_MAIL, JOB_NAME, SHOP_ID) VALUES (:id, :fname, :lname, :phone, :mail, 'Директор', :shop_id)""",
            id=manager_id_array[i],
            fname=fnames.readline().strip(),
            phone=gen(),
            lname=lnames.readline().strip(),
            mail=mails.readline().strip(),
            shop_id=i + 1,
        )
    db.commit()
    db.close()


def gen_usual_employes():
    db = cx_Oracle.connect("ALEXEY", "masterkey", "127.0.0.1:1521/orcl.ofoms.local", encoding="UTF-8")
    cursor = db.cursor()
    fnames = open("surnames.txt", "r", encoding="utf8").readlines()
    lnames = open("names.txt", "r", encoding="utf8").readlines()
    mails = open("mail.txt", "r", encoding="utf8").readlines()
    prof = open("prof.txt", "r", encoding="utf8").readlines()

    random.shuffle(fnames)
    random.shuffle(lnames)
    random.shuffle(mails)

    for i in range(251, 500):
        cursor.execute(
            """INSERT INTO EMPLOYEES (ID, FIRST_NAME, LAST_NAME, PHONE, E_MAIL, JOB_NAME, SHOP_ID) VALUES (:id, :fname, :lname, :phone, :mail, :prof, :shop_id)""",
            id=i + 1,
            fname=fnames[i].strip(),
            phone=gen(),
            lname=lnames[random.randint(0, 101)].strip(),
            mail=mails[i].strip(),
            prof=prof[random.randint(0, 9)],
            shop_id=random.randint(1, 251),
        )
    db.commit()
    db.close()


def gen_products():
    db = cx_Oracle.connect("ALEXEY", "masterkey", "127.0.0.1:1521/orcl.ofoms.local", encoding="UTF-8")
    cursor = db.cursor()
    products = open("products.txt", "r", encoding="utf8")
    pr = products.readlines()
    for i in range(1, 10000):
        cursor.execute("""INSERT INTO PRODUCTS (ID, CODE, NAME) VALUES (:id, sys_guid(), :product)""", id=i, product=pr[random.randint(0, 34)].strip())
    db.commit()
    db.close()