**Описание процедуры создания таблиц и их заполнение данными** 
# Создание таблиц
Создаем таблицу SHOPS (Магазины)  
```SQL
CREATE TABLE SHOPS (
    ID NUMBER(20),
    NAME VARCHAR2(200),
    REGION VARCHAR2(200),
    CITY VARCHAR2(200),
    ADDRESS VARCHAR2(200),
    MANAGER_ID NUMBER(20),
    CONSTRAINT shops_pk PRIMARY KEY (id)
)
```
Создаем таблицу EMPLOYEES (Сотрудники)  
```SQL
CREATE TABLE EMPLOYEES (
    ID NUMBER(20),
    FIRST_NAME VARCHAR2(100),
    LAST_NAME VARCHAR2(100),
    PHONE VARCHAR2(50),
    E_MAIL VARCHAR2(50),
    JOB_NAME VARCHAR2(50),
    SHOP_ID NUMBER(20),
    CONSTRAINT employees_pk PRIMARY KEY (id)
)
``` 
Таблицы связаны внешними ключами, поэтому создаем внешние ключи с отложенными ограничением, чтобы вставка данных была возможна  
Для таблицы EMPLOYEES
``` SQL
ALTER TABLE EMPLOYEES
ADD CONSTRAINT employees_shops_fk
        FOREIGN KEY (SHOP_ID)
        REFERENCES SHOPS(ID)
        DEFERRABLE INITIALLY DEFERRED;
```
Тоже самое для таблицы SHOPS  
```SQL 
ALTER TABLE SHOPS
ADD CONSTRAINT shops_employees_fk
        FOREIGN KEY (MANAGER_ID)
        REFERENCES EMPLOYEES(ID)
        DEFERRABLE INITIALLY DEFERRED;
```
Таблицы EMPLOYEES и SHOPS созданы. Переходим к созаднию других таблицы.  
Создаем таблицу PRODUCTS (Продукты) 
```SQL
CREATE TABLE PRODUCTS (
    ID NUMBER(20),
    CODE VARCHAR2(50),
    NAME VARCHAR2(200),
    CONSTRAINT products_pk PRIMARY KEY (ID),
    CONSTRAINT products_code_uk UNIQUE (CODE)
)
```
Создаем таблицу PURCHASES (Покупки)  
```SQL
CREATE TABLE PURCHASES (
    ID NUMBER(20),
    DATETIME DATE,
    AMOUNT NUMBER(20),
    SELLER_ID NUMBER(20),
    CONSTRAINT purchases_pk PRIMARY KEY (ID),
    CONSTRAINT purchases_employees_fk
        FOREIGN KEY (SELLER_ID)
        REFERENCES EMPLOYEES(ID)
)
```
Создаем таблицу PURCHASE_RECEIPTS (Чеки покупок)
``` SQL
CREATE TABLE PURCHASE_RECEIPTS (
    PURCHASE_ID NUMBER(20),
    ORDINAL_NUMBER NUMBER(5),
    PRODUCT_ID NUMBER(20),
    QUANTITY NUMBER(25,5),
    AMOUNT_FULL NUMBER(20),
    AMOUNT_DISCOUNT NUMBER(20),
    CONSTRAINT purchase_receipts_pk PRIMARY KEY (PURCHASE_ID, PRODUCT_ID),
    CONSTRAINT purchase_receipts_purchases_fk
        FOREIGN KEY (PURCHASE_ID)
        REFERENCES PURCHASES(ID),
    CONSTRAINT purchase_receipts_products_fk
        FOREIGN KEY (PRODUCT_ID)
        REFERENCES PRODUCTS(ID)
)
``` 
Все таблицы созданы  

---  

# Заполнение таблиц данными  
Таблицы SHOPS,  EMPLOYEES и PRODUCTS генерируются запуском скрипта cft_table_filler.py. Сначала таблицы заполняются синхронно. Данные о магазинах и дирректорах синхронизируются.  
Затем табилца сотрудники заполняется обычными сотрудиками. Данные для таблиц взяты из открытых источников интернета. Данные заливаются из файлов txt.  
Таблица PURCHASES заполняется следующим скриптом в Oracle:  
```sql
CREATE OR REPLACE PROCEDURE ALEXEY.FILL_PURCHASES_PR IS
idSeller NUMBER;
randomDate DATE;
randomAmount INTEGER;
BEGIN
   FOR i IN 1 .. 10000 LOOP
   BEGIN
    SELECT SYSDATE-round(DBMS_RANDOM.VALUE (1, 100)) INTO randomDate FROM DUAL; --случайная дата продажи
    SELECT ID INTO idSeller from EMPLOYEES SAMPLE(10) WHERE JOB_NAME = 'Продавец' AND ROWNUM <=1; --случайный id продавца
    SELECT round(DBMS_RANDOM.VALUE (100, 10000)) INTO randomAmount FROM DUAL; --случайная сумма продажи
    INSERT INTO PURCHASES (DATETIME, AMOUNT, SELLER_ID) VALUES (RandomDate, RandomAmount , idSeller);
    EXCEPTION WHEN NO_DATA_FOUND THEN
        BEGIN
            SELECT SYSDATE-round(DBMS_RANDOM.VALUE (1, 100)) INTO randomDate FROM DUAL; --случайная дата продажи
            SELECT NULL INTO idSeller from DUAL; --Покупатель покупает без участия продавца
            SELECT round(DBMS_RANDOM.VALUE (100, 10000)) INTO randomAmount FROM DUAL; --случайная сумма продажи
            INSERT INTO PURCHASES (DATETIME, AMOUNT, SELLER_ID) VALUES (RandomDate, RandomAmount , idSeller);
        END;
   END;
   END LOOP;
   COMMIT;
END FILL_PURCHASES_PR;
```
Вызов процедуры осуществляется путем скрипта:
```sql
DECLARE
BEGIN
FILL_PURCHASES_PR();
END
```
Следующим скриптом заполняем таблицу PURCHASE_RECEIPTS:
```sql
INSERT INTO PURCHASE_RECEIPTS (PURCHASE_ID, ORDINAL_NUMBER, PRODUCT_ID, QUANTITY, AMOUNT_FULL, AMOUNT_DISCOUNT) 
SELECT PURCHASES.ID, --ID покупки
       round(DBMS_RANDOM.VALUE (1, 100)), --порядковый номер в чеке
       1 --временно, будет апдейт процедурой
       round(DBMS_RANDOM.VALUE (1, 5)), --количество товара
       round(DBMS_RANDOM.VALUE (500, 30000)), --Полная стоимость без учета скидки
       1 --временно
FROM PURCHASES SAMPLE(50);

UPDATE PURCHASE_RECEIPTS SET AMOUNT_DISCOUNT = round(PURCHASE_RECEIPTS.AMOUNT_FULL*DBMS_RANDOM.VALUE (1, 25)/100); -- апдейт скидки
```  
Дозаполняем таблицу PURCHASE_RECEIPTS. Проставляем ID проданных товаров случайным образом:  
```sql
CREATE OR REPLACE PROCEDURE FILL_PURCHASE_RECEIPTS IS
cnt NUMBER(5);
CURSOR c1 
    IS 
        SELECT ID FROM (SELECT ID, rownum as R FROM PRODUCTS order by dbms_random.value()) WHERE rownum <= cnt;
id# NUMBER(5);
purchase_id# NUMBER(5);
CURSOR c2 
    IS SELECT PURCHASE_ID FROM PURCHASE_RECEIPTS;
BEGIN
   SELECT COUNT(PRODUCT_ID) INTO cnt FROM PURCHASE_RECEIPTS;
   OPEN c1;
   OPEN c2;
   FOR i IN 1 .. cnt LOOP
    FETCH c1 INTO id#;
    FETCH c2 INTO purchase_id#;
    UPDATE PURCHASE_RECEIPTS SET PRODUCT_ID = id# where PURCHASE_ID = purchase_id#;
   END LOOP;
   CLOSE c1;
   CLOSE c2;
   COMMIT;
   EXCEPTION
     WHEN NO_DATA_FOUND THEN
       NULL;
     WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
       RAISE;
END FILL_PURCHASE_RECEIPTS;
```
Все таблицы заполнены  

---
# Выручка в отчетный период в разрезе регионов  
```sql
SELECT  REGION "РЕГИОН", 
        to_char(SUM(AMOUNT_FULL-AMOUNT_DISCOUNT), '999,999,990.99L', 'NLS_CURRENCY='' руб.''') "ВЫРУЧКА", 
        EXTRACT(MONTH FROM DATETIME) "Номер месяца" 
FROM ALEXEY.PURCHASE_RECEIPTS PR JOIN ALEXEY.PURCHASES PU ON PU.ID = PR.PURCHASE_ID
                                 JOIN EMPLOYEES E ON E.ID = PU.SELLER_ID
                                 JOIN SHOPS S ON S.ID = E.SHOP_ID 
WHERE EXTRACT(MONTH FROM DATETIME) = EXTRACT(MONTH FROM LAST_DAY(SYSDATE)-32) 
  AND EXTRACT(YEAR FROM DATETIME) = EXTRACT(YEAR FROM LAST_DAY(SYSDATE)-32)
GROUP BY REGION, EXTRACT(MONTH FROM DATETIME) 
ORDER BY SUM(AMOUNT_FULL-AMOUNT_DISCOUNT) DESC
```
# Самые эффективные продавцы  
```sql
SELECT
      E.ID,  
      S.NAME, "Название магазина"
      E.FIRST_NAME, "Фамилия"
      E.LAST_NAME "Имя", S.CITY "Город", E.JOB_NAME "Профессия", SUM(AMOUNT_FULL - AMOUNT_DISCOUNT) "Выручка"
FROM ALEXEY.PURCHASE_RECEIPTS PR JOIN ALEXEY.PURCHASES PU ON PU.ID = PR.PURCHASE_ID
                                 JOIN EMPLOYEES E ON E.ID = PU.SELLER_ID
                                                 AND E.JOB_NAME = 'Продавец'
                                 JOIN SHOPS S ON S.ID = E.SHOP_ID 
WHERE EXTRACT(MONTH FROM DATETIME) = EXTRACT(MONTH FROM LAST_DAY(SYSDATE)-32) 
  AND EXTRACT(YEAR FROM DATETIME) = EXTRACT(YEAR FROM LAST_DAY(SYSDATE)-32)
GROUP By E.ID, S.NAME, E.FIRST_NAME, E.LAST_NAME, S.CITY, E.JOB_NAME
ORDER BY SUM(AMOUNT_FULL - AMOUNT_DISCOUNT) DESC
```
# Продавцы, которые ничего не продали в отчетный период  
```sql
SELECT SHOPS.NAME "Название магазина", EMPLOYEES.FIRST_NAME "Фамилия", EMPLOYEES.LAST_NAME "Имя", EMPLOYEES.JOB_NAME "Должность сотрудника" FROM (SELECT ID SELLER_ID FROM EMPLOYEES
                MINUS
               SELECT DISTINCT SELLER_ID FROM PURCHASES WHERE SELLER_ID IS NOT NULL
                                                          AND EXTRACT(MONTH FROM DATETIME) = EXTRACT(MONTH FROM LAST_DAY(SYSDATE)-32)
                                                          AND EXTRACT(YEAR FROM DATETIME) = EXTRACT(YEAR FROM LAST_DAY(SYSDATE)-32) 
              ) NONE_SELLER
         JOIN EMPLOYEES ON EMPLOYEES.ID = NONE_SELLER.SELLER_ID
         JOIN SHOPS     ON SHOPS.ID = EMPLOYEES.SHOP_ID
         WHERE EMPLOYEES.JOB_NAME = 'Продавец'
```
# коды и наименования товаров, которые отсутствовали в отчетный период во всех покупках клиентов
```sql
SELECT CODE "Код", NAME "Наименование продукта"  FROM (SELECT PURCHASE_RECEIPTS.PRODUCT_ID  
                  FROM PURCHASE_RECEIPTS
                MINUS
                SELECT PURCHASE_RECEIPTS.PRODUCT_ID 
                  FROM PURCHASE_RECEIPTS JOIN PURCHASES ON PURCHASES.ID = PURCHASE_RECEIPTS.PURCHASE_ID
                                         JOIN PRODUCTS  ON PRODUCTS.ID = PURCHASE_RECEIPTS.PRODUCT_ID
                 WHERE EXTRACT(MONTH FROM DATETIME) = EXTRACT(MONTH FROM LAST_DAY(SYSDATE)-32) 
                   AND EXTRACT(YEAR FROM DATETIME) = EXTRACT(YEAR FROM LAST_DAY(SYSDATE)-32)
) NONE_PRODUCTS JOIN PRODUCTS ON NONE_PRODUCTS.PRODUCT_ID = PRODUCTS.ID
```

# Отчет о сбое  
```sql
SELECT SHOPS.NAME "Название магазина", DATETIME "Дата сбоя", ABS(AMOUNT- (AMOUNT_FULL-AMOUNT_DISCOUNT)) "Cумма расхождения"
FROM PURCHASE_RECEIPTS JOIN PRODUCTS  ON PRODUCTS.ID = PURCHASE_RECEIPTS.PRODUCT_ID
                       JOIN PURCHASES ON PURCHASES.ID = PURCHASE_RECEIPTS.PURCHASE_ID
                       JOIN EMPLOYEES ON EMPLOYEES.ID = PURCHASES.SELLER_ID
                       JOIN SHOPS     ON SHOPS.ID = EMPLOYEES.SHOP_ID
WHERE AMOUNT_FULL - AMOUNT_DISCOUNT <> AMOUNT
ORDER BY DATETIME, SHOPS.NAME, ABS(AMOUNT- (AMOUNT_FULL-AMOUNT_DISCOUNT))
```